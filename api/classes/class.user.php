<?php
	
/**
	* @author Himanshu Choudhary
	* @email himanshuchoudhary@live.com
*/

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class User {

	public function signup(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;

		$params = $request->getParsedBody();
		$data = [];

		$uname = $params['username'];
		$pass = $params['password'];
		$pub = ($params['is_pub'])?1:0;

		$query = "INSERT INTO users (username, password, publisher) VALUES ('$uname', '$pass', $pub)";
		$db->executeQuery($query);

		if($db->error){
			$data['success'] = FALSE;
			$data['message'] = 'Error in inserting into database. Error : '.$db->err_mess;
			$data['data'] = NULL;
		}
		else {
			$generated_uid = $db->lastInsertId();
			
			$data['success'] = TRUE;
			$data['message'] = 'Successfully registered';
			$data['data'] = array(
				'id' => $generated_uid,
				'username' => $uname
			);
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function login(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db, $app;

		$params = $request->getParsedBody();
		$data = [];

		$uname = $params['username'];
		$pass = $params['password'];

		$query = "SELECT id,username,publisher,last_active,last_login,last_logout FROM users WHERE username='$uname' AND password='$pass'";
		$res = $db->executeQuery($query);

		if($db->error){
			$data['success'] = FALSE;
			$data['message'] = 'Error : '.$db->err_mess;
			$data['data'] = NULL;
		}
		else {			
			if($res->num_rows > 0){
				$data['success'] = TRUE;
				$data['message'] = 'Successfully logged in.';
				$data['data'] = $res->fetch_assoc();

				unset($_SESSION['loggedin']);
				$_SESSION['loggedin'] = 1;
				$_SESSION['USERDATA'] = $data['data'];
				$_SESSION['USERDATA']['last_active'] = date('Y-m-d H:i:s',time());
				$_SESSION['USERDATA']['last_login'] = date('Y-m-d H:i:s',time());
				$this->updateLastLogin();
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'Incorrect Username/Password.';
				$data['data'] = NULL;
			}
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function logout(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		$this->updateLastLogout();
		session_unset();
		session_destroy();
		$data = [];
		if (!isset($_SESSION['loggedin'])){
			$data['success'] = TRUE;
			$data['message'] = 'Logged out succesfully.';
			$data['data'] = NULL;
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Some error occurred.';
			$data['data'] = NULL;
		}
		$response->write(json_encode($data));
	    return $response;
	}

	public function checkAuth(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		$data = [];
		if ($_SESSION['loggedin'] == 1){
			$this->updateLastActive();
			$_SESSION['USERDATA']['last_active'] = date('Y-m-d H:i:s',time());
			$data['success'] = TRUE;
			$data['msg'] = 'User logged in.';
			$data['data'] = $_SESSION['USERDATA'];
		}
		else {
			$data['success'] = FALSE;
			$data['msg'] = 'User not logged in.';
			$data['data'] = NULL;
		}
		$response->write(json_encode($data));
	    return $response;
	}

	private function updateLastActive()
	{
		Global $db;
		$curr_time = date('Y-m-d H:i:s',time());
		$user = $_SESSION['USERDATA']['id'];
		$query = "UPDATE users SET last_active = '$curr_time' WHERE id='$user'";
		$db->executeQuery($query);
	}

	private function updateLastLogin()
	{
		Global $db;
		$curr_time = date('Y-m-d H:i:s',time());
		$user = $_SESSION['USERDATA']['id'];
		$query = "UPDATE users SET last_login = '$curr_time' WHERE id='$user'";
		$db->executeQuery($query);
	}

	private function updateLastLogout()
	{
		Global $db;
		$curr_time = date('Y-m-d H:i:s',time());
		$user = $_SESSION['USERDATA']['id'];
		$query = "UPDATE users SET last_logout = '$curr_time' WHERE id='$user'";
		$db->executeQuery($query);
	}

	public function updateUsername(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;

		$data = [];

		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']){
			$username = $_SESSION['USERDATA']['username'];
			$params = $request->getParsedBody();
			$new_uname = $params['newuname'];

			$query = "SELECT id FROM users WHERE username='$new_uname'";
			$res = $db->executeQuery($query);
			if($res->num_rows == 0){
				$flag = TRUE;
				if($db->executeQuery($query))
				$upquery = "UPDATE users u SET u.username = '$new_uname' WHERE u.username = '$username';
					UPDATE channels c SET c.pub = '$new_uname' WHERE c.pub = '$username';
					UPDATE subscription s SET s.sub = '$new_uname' WHERE s.sub = '$username';
					UPDATE articles a SET a.pub = '$new_uname' WHERE a.pub = '$username';";
				
				$upres = $db->executeMultiQuery($upquery);
				if($upres) {
					$data['success'] = TRUE;
					$data['message'] = 'Username updated.';
					$data['data'] = NULL;
					$_SESSION['USERDATA']['username'] = $new_uname;
				}
				else {
					$data['success'] = FALSE;
					$data['message'] = 'Some error occurred. Error : '.$db->err_mess;
					$data['data'] = NULL;
				}
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'Username already taken.';
				$data['data'] = NULL;
			}
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}
}
?>