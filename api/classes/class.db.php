<?php
	
/**
	* @author Himanshu Choudhary
	* @email himanshuchoudhary@live.com
*/

class Database {

	private $host      = DB_HOSTNAME;
	private $user      = DB_USERNAME;
	private $pass      = DB_PASSWORD;
	private $dbname    = DB_DATABASE;
	private $conn;

	public $query  		= NULL;
	public $result		= NULL;
	public $error		= NULL;
	public $err_mess	= NULL;

	public function __construct() 
	{	
		$this->conn = new mysqli($this->host, $this->user, $this->pass, $this->dbname);

		if ($this->conn->connect_error){
			$this->error = TRUE;
			$this->err_mess = $this->conn->connect_error();
	  	}
	  	else {
	  		$this->error = FALSE;
	  	}
	}

	public function executeQuery($query)
	{
		$this->query = $query;
		$res = $this->conn->query($query);
		if ($res){
			$this->error = FALSE;
			$this->result = $res;
		}
		else {
			$this->error = TRUE;
			$this->err_mess = $this->conn->error;
			$this->result = NULL;
		}
		return $res;
	}

	public function executeMultiQuery($query)
	{
		$this->query = $query;
		$res = $this->conn->multi_query($query);
		if ($res){
			$this->error = FALSE;
			$this->result = $res;
		}
		else {
			$this->error = TRUE;
			$this->err_mess = $this->conn->error;
			$this->result = NULL;
		}
		return $res;
	}

	public function lastInsertId()
	{
		return $this->conn->insert_id;
	}

	public function closeConnection()
	{
		$this->conn->close();
	}
}
?>