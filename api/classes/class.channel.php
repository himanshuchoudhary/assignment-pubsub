<?php
	
/**
	* @author Himanshu Choudhary
	* @email himanshuchoudhary@live.com
*/

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class Channel {

	public function createChannel(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;
		$data = [];
		
		if(isset($_SESSION['USERDATA']) && $_SESSION['USERDATA']['publisher']){
			$params = $request->getParsedBody();

			$ch_name = $params['name'];
			$ch_pub = $_SESSION['USERDATA']['username'];

			$query = "INSERT INTO channels (name, pub) VALUES ('$ch_name','$ch_pub')";
			$res = $db->executeQuery($query);
			
			if($db->error){
				$data['success'] = FALSE;
				$data['message'] = 'Error : '.$db->err_mess;
				$data['data'] = NULL;
			}
			else {
				$generated_cid = $db->lastInsertId();
				
				$data['success'] = TRUE;
				$data['message'] = 'Channel created successfully.';
				$data['data'] = array(
					'id' => $generated_cid,
					'name' => $ch_name,
					'publisher' => $ch_pub
				);
			}
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function getChannels(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;
		$data = [];
		
		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']){
			$username = $_SESSION['USERDATA']['username'];
			$query = "SELECT channels.id,channels.name,channels.pub,channels.created_on, subscription.sub FROM channels LEFT OUTER JOIN subscription ON id = chid AND sub = '$username' ORDER BY id DESC";
			$res = $db->executeQuery($query);
			
			if($res){
				$data['success'] = TRUE;
				$data['message'] = 'Fetched channels list.';
				
				while ($row = $res->fetch_assoc()) {
					$data['data'][] = array(
						'id' => $row['id'],
						'name' => $row['name'],
						'publisher' => $row['pub'],
						'created_on' => $row['created_on'],
						'is_pub' => ($row['pub'] == $_SESSION['USERDATA']['username'])?1:0,
						'is_new' => ($row['created_on'] > $_SESSION['USERDATA']['last_logout'])?1:0,
						'is_sub' => ($row['sub'])?1:0
					);
				}
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'No channels available.';
				$data['data'] = NULL;
			}			
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function subscribeChannel(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;

		$data = [];

		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']){
			$params = $request->getParsedBody();

			$chid = $params['chid'];
			$username = $_SESSION['USERDATA']['username'];

			$query = "INSERT INTO subscription (chid, sub) VALUES ($chid, '$username')";
			$res = $db->executeQuery($query);
			
			if($res){
				$data['success'] = TRUE;
				$data['message'] = 'Subscribed.';
				$data['data'] = NULL;
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'Error : '.$db->err_mess;
				$data['data'] = NULL;
			}
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function unsubscribeChannel(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;

		$data = [];

		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']){
			$params = $request->getParsedBody();

			$chid = $params['chid'];
			$username = $_SESSION['USERDATA']['username'];

			$query = "DELETE FROM subscription WHERE chid = $chid AND sub = '$username'";
			$res = $db->executeQuery($query);
			
			if($res){
				$data['success'] = TRUE;
				$data['message'] = 'Unsubscribed.';
				$data['data'] = NULL;
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'Error : '.$db->err_mess;
				$data['data'] = NULL;
			}
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function submitArticle(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;

		$data = [];

		if(isset($_SESSION['USERDATA']) && $_SESSION['USERDATA']['publisher']){
			$params = $request->getParsedBody();

			$title = $params['title'];
			$content = $params['content'];
			$channel = $params['chid'];
			$username = $_SESSION['USERDATA']['username'];

			$query = "INSERT INTO articles (title,content,chid,pub) VALUES ('$title','$content',$channel,'$username')";
			$res = $db->executeQuery($query);

			if($res){
				$data['success'] = TRUE;
				$data['message'] = 'Article published successfully.';
				$data['data'] = NULL;
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'Error : '.$db->err_mess;
				$data['data'] = NULL;
			}
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function getAllArticles(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;

		$data = [];

		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']){
			$username = $_SESSION['USERDATA']['username'];

			$query = "SELECT articles.id, articles.title, articles.content, articles.chid, articles.pub, articles.created_on, channels.name FROM articles JOIN channels ON channels.id = articles.chid WHERE EXISTS (SELECT chid FROM subscription WHERE subscription.chid = articles.chid AND subscription.sub = '$username') OR articles.pub = '$username' ORDER BY created_on DESC";

			$res = $db->executeQuery($query);

			if($res){
				$data['success'] = TRUE;
				$data['message'] = 'New articles.';
				
				while ($row = $res->fetch_assoc()) {
					$data['data'][] = array(
						'id' => $row['id'],
						'title' => $row['title'],
						'content' => $row['content'],
						'publisher' => $row['pub'],
						'created_on' => $row['created_on'],
						'channel_name' => $row['name'],
						'channel_id' => $row['chid'],
						'is_pub' => ($row['pub'] == $_SESSION['USERDATA']['username'])?1:0,
						'is_new' => ($row['created_on'] > $_SESSION['USERDATA']['last_logout'])?1:0,
						'is_sub' => ($row['pub'] != $_SESSION['USERDATA']['username'])?1:0,
						'is_bnew' => 0
					);
				}
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'Error : '.$db->err_mess;
				$data['data'] = NULL;
			}
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}

	public function getNewArticles(ServerRequestInterface $request,ResponseInterface $response,array $args)
	{
		Global $db;

		$data = [];

		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']){
			$username = $_SESSION['USERDATA']['username'];
			$count = 0;
			session_write_close();
			while($count < 100){
				$last_active = $_SESSION['USERDATA']['last_active'];
				$query = "SELECT articles.id, articles.title, articles.content, articles.chid, articles.pub, articles.created_on, channels.name FROM articles JOIN channels ON channels.id = articles.chid WHERE (EXISTS (SELECT chid FROM subscription WHERE subscription.chid = articles.chid AND subscription.sub = '$username') OR articles.pub = '$username') AND articles.created_on >= '$last_active'  ORDER BY created_on DESC";
				$res = $db->executeQuery($query);
				if($res->num_rows > 0){
					break;
				}
				sleep(10);
				clearstatcache();
				$count++;
			}
			session_start();
			if($res){
				$_SESSION['USERDATA']['last_active'] = date('Y-m-d H:i:s',time());
				$data['success'] = TRUE;
				$data['message'] = 'Fetched articles.';
				if($res->num_rows == 0){
					$data['data'] = NULL;
				}
				
				while ($row = $res->fetch_assoc()) {
					$data['data'][] = array(
						'id' => $row['id'],
						'title' => $row['title'],
						'content' => $row['content'],
						'publisher' => $row['pub'],
						'created_on' => $row['created_on'],
						'channel_name' => $row['name'],
						'channel_id' => $row['chid'],
						'is_pub' => ($row['pub'] == $_SESSION['USERDATA']['username'])?1:0,
						'is_new' => ($row['created_on'] > $_SESSION['USERDATA']['last_logout'])?1:0,
						'is_sub' => ($row['pub'] != $_SESSION['USERDATA']['username'])?1:0,
						'is_bnew' => 1
					);
				}
			}
			else {
				$data['success'] = FALSE;
				$data['message'] = 'Request timeout.';
				$data['data'] = NULL;
			}
		}
		else {
			$data['success'] = FALSE;
			$data['message'] = 'Not authorized.';
			$data['data'] = NULL;
		}

		$response->write(json_encode($data));
	    return $response;
	}
}