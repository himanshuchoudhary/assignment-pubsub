<?php
	
/**
	* @author Himanshu Choudhary
	* @email himanshuchoudhary@live.com
*/

	error_reporting(-1);
	ini_set('display_errors', 'On');

	session_name('pubsub');
	session_start();
	if(!isset($_SESSION['loggedin'])){
		$_SESSION['loggedin'] = 0;
	};

	define('APP_BASE', 'http://local.pubsub.com/');
	define('API_BASE', 'http://local.pubsub.com/api/');

	include 'db_config.php';
	include 'classes/class.db.php';
	include 'classes/class.user.php';
	include 'classes/class.channel.php';

	require 'vendor/autoload.php';

	$app = new Slim\App();
	$db = new Database();

?>