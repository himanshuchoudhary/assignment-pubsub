<?php

	include 'config/globals.php';

	$app->get('/', function ($request, $response, $args) {
	    $response->write("Hello World");
	    return $response;
	});

	$app->post('/loginUser','User:login');
	$app->post('/signupUser','User:signup');
	$app->get('/logoutUser','User:logout');
	$app->get('/checkAuth','User:checkAuth');
	$app->post('/updateUsername','User:updateUsername');

	$app->post('/createChannel','Channel:createChannel');
	$app->get('/getChannels','Channel:getChannels');
	$app->post('/subscribeChannel','Channel:subscribeChannel');
	$app->post('/unsubscribeChannel','Channel:unsubscribeChannel');
	$app->post('/submitArticle','Channel:submitArticle');
	$app->get('/getAllArticles','Channel:getAllArticles');
	$app->get('/getNewArticles','Channel:getNewArticles');

	$app->run();
?>