
var app = angular.module('pubsub', ['ngRoute','notification']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/login', {
            templateUrl: '/assets/view/login.html',
            controller: 'LoginController'
        }).
        when('/logout', {
            templateUrl: '/assets/view/login.html',
            controller: 'LogoutController'
        }).
        when('/home', {
            templateUrl: '/assets/view/home.html',
            controller: 'HomeController'
        }).
        otherwise({
            redirectTo: '/login'
        });
}]);

app.constant('USER_DATA',{
    is_loggedin : false,
    id : null,
    username : null,
    role : 'sub'
});

app.run(function($rootScope, $location, USER_DATA, UserService) {
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
    	UserService.checkLogin().then(function(response){
	      	if (response == false) {
	      		$rootScope.username = null;
	          	$location.path("/login");
	      	}
	      	else {
                $rootScope.role = USER_DATA.role;
	      		$rootScope.username = USER_DATA.username;
	      		if (next.templateUrl === "/assets/view/login.html"){
		          	$location.path("/home");
	      		}
	      	}
	    });      	
    });
});