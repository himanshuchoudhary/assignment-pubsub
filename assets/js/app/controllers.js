
app.controller('LoginController', function($rootScope,$scope,$location,LoginService,USER_DATA){
	$scope.signup_ispub = false;
	$scope.submitLogin = function(){
		LoginService.loginUser($scope.login_uname, $scope.login_pass).then(function(response){
			$scope.msg = response.message;
			if(response.success == true){
				USER_DATA.is_loggedin = true;
				USER_DATA.id = response.data.id;
				USER_DATA.username = response.data.username;
				if(response.data.publisher == 1){
					USER_DATA.role = 'pub';
				}
				$location.path("/home");
			}
		});
	};
	$scope.submitSignup = function(){
		LoginService.signupUser($scope.signup_uname, $scope.signup_pass, $scope.signup_ispub).then(function(response){
			$scope.msg = response.message;
		});
	};
});

app.controller('LogoutController', function($rootScope,$location,LoginService,USER_DATA){
	LoginService.logoutUser().then(function(response){
		if(response.success == true){
			USER_DATA.is_loggedin = false;
			$location.path("/login");
		}
		else {
			alert(response.message);
		}
	});
});

app.controller('HomeController', function($scope,$location,$timeout,$route,USER_DATA,UserService,$notification){
	
	$scope.articles = [];
	
	var getChannels = function(){
		UserService.getChannels().then(function(response){
			$scope.channels = response.data;
		});
	}

	var getAllArticles = function(){
		UserService.getAllArticles().then(function(response){
			if(response.success == true){
				$scope.articles = response.data;
			}
			else {
				$scope.msg = response.message;
			}
		});
	}

	var getNewArticles = function(){
		UserService.getNewArticles().then(function(response){
			if (response.success == true){
				if(response.data){
					if(!$scope.articles){
						$scope.articles = [];
					}
					for (var i = 0; i < response.data.length; i++){
						$scope.articles.unshift(response.data[i]);
						var noti_title = '['+response.data[i].channel_name+']'+' New article : '+response.data[i].title;
						var notification = $notification(noti_title, {
						    body: response.data[i].content
					  	});
					  	$timeout(function(){
					  		notification.close();
					  	},30000);
					}
					$scope.msg = response.message;
				}
			}
			else {
				$scope.msg = response.message;
			}
			$timeout(getNewArticles,5000);
		});
	}

	$scope.new_channel = {};
	$scope.new_article = {};
	$scope.curr_path = $location.path();
	
	$scope.createChannel = function(){
		if($scope.new_channel.name){
			UserService.createChannel($scope.new_channel.name).then(function(response){
				$scope.msg = response.message;
				getChannels();
			});
		}
		else {
			$scope.msg = 'Please enter some name.';
			return false;
		}
	};

	$scope.subscribeCh = function(cid){
		UserService.subscribeChannel(cid).then(function(response){
			if(response.success == true){
				getChannels();
			}
			$scope.msg = response.message;

		});
	}

	$scope.unsubscribeCh = function(cid){
		UserService.unsubscribeChannel(cid).then(function(response){
			if(response.success == true){
				getChannels();
			}
			$scope.msg = response.message;

		});
	}

	$scope.submitArticle = function(){
		if ($scope.new_article.title && $scope.new_article.content && $scope.new_article.channel){
			UserService.submitArticle($scope.new_article.title,$scope.new_article.content,$scope.new_article.channel).then(function(response){
				$scope.msg = response.message;
			});
		}
		else {
			$scope.msg = "Invalid inputs."
			return false;
		}
	}

	$scope.updateUsername = function(){
		if($scope.new_username){
			UserService.updateUsername($scope.new_username).then(function(response){
				if(response.success == true){
					$route.reload();
				}
				$scope.msg = response.message;
			});
		}
		else {
			$scope.msg = 'Enter some username';
		}
	}

	getChannels();
	getAllArticles();
	$timeout(getNewArticles,2000);
});