
app.service('LoginService', function($http, $q){
	this.loginUser = function(uname, pass){
		var deferred = $q.defer();
		$http.post('/api/loginUser', {username : uname, password : pass}).then(function(response){
			deferred.resolve(response.data);
		});
		return deferred.promise;
	}
	this.signupUser = function(uname, pass, pub){
		var deferred = $q.defer();
		$http.post('/api/signupUser', {username : uname, password : pass, is_pub : pub}).then(function(response){
			deferred.resolve(response.data);
		});
		return deferred.promise;
	}
	this.logoutUser = function(){
		var deferred = $q.defer();
		$http.get('/api/logoutUser').then(function(response){
			deferred.resolve(response.data);
		});
		return deferred.promise;
	}
});



app.service('UserService', function($location,$http,$q,USER_DATA){
	this.checkLogin = function(){
		var deferred = $q.defer();
		$http.get('/api/checkAuth').success(function(response){
			if (response.success == true) {
				USER_DATA.is_loggedin = true;
				USER_DATA.is = response.data.id;
				USER_DATA.username = response.data.username;
				if(response.data.publisher == 1){
					USER_DATA.role = 'pub';
				}
				deferred.resolve(true);
			}
			deferred.resolve(false);
		});
		return deferred.promise;
	}

	this.updateUsername = function(uname){
		var deferred = $q.defer();
		$http.post('/api/updateUsername',{newuname : uname}).success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}

	this.createChannel = function(name){
		var deferred = $q.defer();
		$http.post('/api/createChannel',{name : name}).success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}

	this.getChannels = function(){
		var deferred = $q.defer();
		$http.get('/api/getChannels').success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}

	this.subscribeChannel = function(cid){
		var deferred = $q.defer();
		$http.post('/api/subscribeChannel',{chid : cid}).success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}

	this.unsubscribeChannel = function(cid){
		var deferred = $q.defer();
		$http.post('/api/unsubscribeChannel',{chid : cid}).success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}

	this.submitArticle = function(title,content,channel){
		var deferred = $q.defer();
		$http.post('/api/submitArticle',{title : title, content : content, chid : channel}).success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}

	this.getAllArticles = function(){
		var deferred = $q.defer();
		$http.get('/api/getAllArticles').success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}

	this.getNewArticles = function(){
		var deferred = $q.defer();
		$http.get('/api/getNewArticles').success(function(response){
			deferred.resolve(response);
		});
		return deferred.promise;
	}
});